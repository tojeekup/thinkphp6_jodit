<?php
define('JODIT_DEBUG', false);

if (!function_exists('jodit')) {
	function jodit(){
		$config = config('jodit.');
		require_once __DIR__ . '/Application.php';
		$fileBrowser = new \JoditRestApplication($config);
	
		try {
			$fileBrowser->checkAuthentication();
			$fileBrowser->execute();
		} catch(\ErrorException $e) {
			$fileBrowser->exceptionHandler($e);
		}
	
	}
}

